## Wordpress plugins Translation

### Create .po file with [POEdit](https://poedit.net/)
1. File -> Catalogs Manager -> New
    * give it a name
    * add the base path of the plugin
2. File -> New -> Choose language
3. Catalog -> Properties -> Translation Properties
    * Add a Project Name
    * Charset: UTF-8 (default)
    * Source code charset: UTF-8 (on unix you need to set this as well)
    * Team & Email can be completed as well
4. Catalog -> Properties -> Sources Keywords
    * Click on the second icon and then add: `__`, `_e`, `_n` (read about [plurals](#add-support-for-plural-forms)), `_x`, `_ex`
5. File -> Save -> Enter a file name and choose a location
6. Catalog -> Properties -> Sources Paths
    * under Paths add the base folder of the plugin
    * under Excluded Paths add the *assets* folder from the plugin
7. Save file and press update
    * file is ready for translations now
    * can be saved, renamed to .pot and added to plugins

##### Notes
    * .po file is exactly the same thing as .pot
    * .pot files can be used as a starting point when creating other translations, so these should be provided
    * you just rename a .po file to get them
    * .po stands for a format that is readable by humans, while .mo stands for a format that is readable by machines

### Add support for plural forms
* The plural format is automatically selected when selecting the language that you are translating into
* All you have to do to make the .po file support plurals is to go to the Sources Keywords Tab -> New and add/edit the plural function like this `_n:1,2` where `_n` is the Wordpress function which helps you translate plurals and `:1,2` specifies the number of plurals the current language has.

### Translation functions used in Wordpress
* [__](https://developer.wordpress.org/reference/functions/__/) - standard translation functions
* [_e](https://developer.wordpress.org/reference/functions/_e/) - same but prints the text as well
* [_n](https://codex.wordpress.org/Function_Reference/_n) - allows you to add support for plurals when translating (based on a compare value)
* [_x](https://codex.wordpress.org/Function_Reference/_x) - allows you to give context to the text that you make available for translation
* [_ex](https://codex.wordpress.org/Function_Reference/_ex) - same as above, but it prints the text as well.
