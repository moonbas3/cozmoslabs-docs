## Hooks available in Passwordless Login

#### Email
* **Filter**: `wpa_email_subject`
* Change the subject of the Email that is sent

* **Filter**: `wpa_email_message`
* Change the content of the Email Message that is getting sent (supports HTML)

#### Link Duration
* **Filter**: `wpa_change_link_expiration`
* Change the expiration time of the link that is getting sent.

#### Plugin Messages
* **Filter**: `wpa_success_link_msg`
* Change the success message that is shown if the link is sent successfully

* **Filter**: `wpa_success_login_msg`
* Change the message that is displayed after the user is logged in

* **Filter**: `wpa_error`
* Change the message that is displayed in case the link couldn't be sent

* **Filter**: `wpa_invalid_token_error`
* Change the message that is displayed if the token is invalid

#### Login Form
* **Filter**: `wpa_change_form_label`
* Change the label from the Login form.

* **Action**: `wpa_login_form`
* Executed before the nonce and ending </form> tag in the login form
